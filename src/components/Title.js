import React from 'react';

const Title = (props) => (
    <div className="section-title">
        <h4>{props.title}</h4>
        <div></div>
    </div>
);

export default Title;