import React from 'react';

const Hero = (props) => (
    <header className={props.hero}>
        {props.children}
    </header>
);

export default Hero;