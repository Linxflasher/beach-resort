import React, { Component } from "react";
import Title from "./Title";
import { FaCocktail, FaShuttleVan, FaHiking, FaBeer } from "react-icons/fa";

class Services extends Component {
  state = {
    services: [
      {
        icon: <FaCocktail></FaCocktail>,
        title: "Free Cocktails",
        info:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi, explicabo vel nihil, sint beatae rerum, eum provident incidunt amet optio facilis. Alias sint molestias quas nemo a dolores, quia perspiciatis!"
      },
      {
        icon: <FaHiking></FaHiking>,
        title: "Endless Hiking",
        info:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi, explicabo vel nihil, sint beatae rerum, eum provident incidunt amet optio facilis. Alias sint molestias quas nemo a dolores, quia perspiciatis!"
      },
      {
        icon: <FaShuttleVan></FaShuttleVan>,
        title: "Free Shuttle",
        info:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi, explicabo vel nihil, sint beatae rerum, eum provident incidunt amet optio facilis. Alias sint molestias quas nemo a dolores, quia perspiciatis!"
      },
      {
        icon: <FaBeer></FaBeer>,
        title: "Strongest Beer",
        info:
          "Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi, explicabo vel nihil, sint beatae rerum, eum provident incidunt amet optio facilis. Alias sint molestias quas nemo a dolores, quia perspiciatis!"
      }
    ]
  };
  render() {
    return (
      <section className="services">
        <Title title="Services"></Title>
        <div className="services-center">
          {this.state.services.map((item, index) => {
            return (
              <article key={index} className="service">
                <span>{item.icon}</span>
                <h6>{item.title}</h6>
                <p>{item.info}</p>
              </article>
            );
          })}
        </div>
      </section>
    );
  }
}

export default Services;
