import React from "react";
import Hero from "../components/Hero";
import Banner from "../components/Banner";
import { Link } from "react-router-dom";
import Services from "../components/Services";
import FeaturedRooms from "../components/FeaturedRooms";

const Home = () => (
  <React.Fragment>
    <Hero hero="defaultHero">
      <Banner title="Luxurious rooms" subtitle="deluxe rooms starting at $299">
        <Link className="btn-primary" to="/rooms">
          our rooms
        </Link>
      </Banner>
    </Hero>
    <Services></Services>
    <FeaturedRooms></FeaturedRooms>
  </React.Fragment>
);

export default Home;
